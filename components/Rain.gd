extends Node2D

export (PackedScene) var Raindrop
export var drop_interval = 1.0
export var drop_angle = 0.0


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$RaindropSpawnTimer.wait_time = drop_interval
	$RaindropSpawnTimer.start()


func _on_RaindropSpawnTimer_timeout() -> void:
	$RaindropSpawnTimer.wait_time = drop_interval
	$RaindropSpawnPath/PathFollow2D.offset = randi()
	var raindrop = Raindrop.instance()
	raindrop.z_index = (randi() % 2)*1000 - 500
	$Raindrops.add_child(raindrop)
	raindrop.position = $RaindropSpawnPath/PathFollow2D.global_position
	raindrop.rotation_degrees = drop_angle
