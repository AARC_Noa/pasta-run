extends Node

onready var timer: Timer = $Manager/TimeLeft
onready var timer_text: Label = $HudLayer/VBoxContainer/TimeLeft

onready var timeLeft = $Manager/TimeLeft.wait_time
var bonus_time_per_pasta = 5.0

var current_map = null
onready var gameEnd = false

var objective: Area2D
var totalObjectiveCount: int
var rng = RandomNumberGenerator.new()

const MAP_PREFIX = "res://maps/"
const MAPS = ["map0.tscn", "map1.tscn"]

func _ready() -> void:
	rng.randomize()
	var picked_map: PackedScene = load(MAP_PREFIX + MAPS[rng.randi_range(0, MAPS.size()-1)])
	var instance = picked_map.instance()
	add_child(instance)
	current_map = instance
	totalObjectiveCount = current_map.get_node("targets").get_child_count()
	bonus_time_per_pasta = 10.0/(totalObjectiveCount/2.0)
	print("bonus time per pasta: " + str(bonus_time_per_pasta))
	$RigidPlayer.position = instance.get_node("Spawn").position
	$MusicManager/MainMusic.play(0)

func _process(delta: float) -> void:
	if (!timer.is_stopped()):
		timer_text.text = "Time left: " + ("%.1f" % timer.time_left) + "s"
		
	if(timer.time_left < timeLeft/2 && !gameEnd):
		if(timer.time_left < timeLeft/4):
			$MusicManager/MainMusic.pitch_scale = 2
		else:
			$MusicManager/MainMusic.pitch_scale = 1.5
	else:
		$MusicManager/MainMusic.pitch_scale = 1
		
	if current_map.get_node("targets").get_child_count() == 0 && !gameEnd :
		gaeming()

func gaeming():
	timer.stop()
	timer_text.text = "Gaeming"
	$SFX/Win.play()
	$HudLayer/game_message.text = "Pasta secured"
	$HudLayer/game_message.visible = true
	$Manager/SystemTimer.start()
	$MusicManager/MainMusic.stop()
	print($Manager/SystemTimer)
	gameEnd = true

func _on_TimeLeft_timeout() -> void:
	timer_text.text = "Time over"
	$SFX/Fail.play()
	$HudLayer/game_message.text = "Pasta burnt"
	$HudLayer/game_message.visible = true
	$Manager/SystemTimer.start()
	$MusicManager/MainMusic.stop()
	print($Manager/SystemTimer)
	gameEnd = true
	

func _on_RigidPlayer_picking_node(node):
	if (gameEnd):
		return
	node.get_node('pickupAnimation').play("picked")
	var remaining = current_map.get_node("targets").get_child_count()
	var pitch_shift = ((totalObjectiveCount - remaining) / float(totalObjectiveCount)) * 0.5
	$SFX/PastaCollected.pitch_scale = 1 + pitch_shift
	$SFX/PastaCollected.play()
	timer.wait_time = timer.time_left + bonus_time_per_pasta
	timer.start()


func _on_SystemTimer_timeout():
	get_tree().reload_current_scene()
