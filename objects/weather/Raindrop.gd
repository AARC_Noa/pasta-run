extends Sprite

export var speed = 900.0

func _ready() -> void:
	$LifetimeTimer.start()

func _process(delta: float) -> void:
	position += Vector2(0, speed*delta).rotated(deg2rad(rotation_degrees))


func _on_LifetimeTimer_timeout() -> void:
	queue_free()
