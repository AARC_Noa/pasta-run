extends RigidBody2D

signal picking_node(node)

# Character properties
export var acceleration = 200.0
export var sprint_boost = 1.5
export var jump_force = 800.0
export var top_move_speed = 400.0
export var top_jump_speed = 800.0

const MAX_JUMPS = 2
const TOP_JUMP_TIME = 0.7
const WALL_HUG_FACTOR = 0.02

var hug_left = false
var hug_right = false

var fell = false
var sprinting = false
var grounded = false
var is_on_wet_ground = false
var can_jump = false
var jump_time = 9
var allowed_jumps = MAX_JUMPS

var directional_force = Vector2()
const DIRECTION = {
	ZERO = Vector2(0,0),
	LEFT = Vector2(-1,0),
	RIGHT = Vector2(1,0),
	UP = Vector2(0,-1),
	DOWN = Vector2(0,1),
}

# Check movement input here
func apply_force(state):
	if !fell:
		sprinting = Input.is_action_pressed("sprint")		
	
		if Input.is_action_pressed("ui_left"):
			directional_force += DIRECTION.LEFT * (WALL_HUG_FACTOR if hug_left else 1)		
			
		if Input.is_action_pressed("ui_right"):
			directional_force += DIRECTION.RIGHT * (WALL_HUG_FACTOR if hug_right else 1)
			
		if sprinting and !(hug_left or hug_right):
			directional_force *= sprint_boost
		
		if Input.is_action_just_pressed("ui_select") and (allowed_jumps > 0 or (allowed_jumps <= 0 and !grounded and (hug_left or hug_right))):		
			$SFX/Jump.pitch_scale = rand_range(0.9,1.1)
			$SFX/Jump.play()
			directional_force += DIRECTION.UP
			if !grounded:
				if hug_left:   
					directional_force += 2*DIRECTION.RIGHT
				elif hug_right:
					directional_force += 2*DIRECTION.LEFT
		elif Input.is_action_just_released("ui_select"):
			allowed_jumps -= 1
	
	if grounded:
		allowed_jumps = MAX_JUMPS

# Called on each physics frame - finds ground tile, applies forces
func _integrate_forces(state: Physics2DDirectBodyState) -> void:
	process_movement(state)
	process_fall(state)

func process_movement(state):
	# Final force
	var final_force = Vector2()
	
	# Not moving when no direction
	directional_force = DIRECTION.ZERO
	
	# Apply force on character
	apply_force(state)
	
	# Get movement velocity
	final_force = state.linear_velocity + directional_force * Vector2(acceleration, 0)
	
	if directional_force.y != 0:
		final_force.y = directional_force.y * jump_force
	
	var local_tms = top_move_speed * (sprint_boost if sprinting else 1)
	
	# prevent from exceeding top speeds
	if final_force.x > local_tms:
		final_force.x = local_tms
	elif final_force.x < -local_tms:
		final_force.x = -local_tms
		
	if final_force.y > top_jump_speed:
		final_force.y = top_jump_speed
	elif final_force.y < -top_jump_speed:
		final_force.y = -top_jump_speed
		
	# Add force
	state.linear_velocity = final_force
	
func process_fall(state: Physics2DDirectBodyState):
	# Moving, sprinting, on wet ground
	if directional_force.x != 0 and sprinting and is_on_wet_ground:
		$SFX/Trip.pitch_scale = rand_range(0.9,1.1)
		$SFX/Trip.play()
		fell = true
		set_deferred("mode", RigidBody2D.MODE_RIGID)
		yield(get_tree().create_timer(rand_range(2.0,2.5)), "timeout")
		set_deferred("mode", RigidBody2D.MODE_CHARACTER)
		rotation_degrees = 0
		fell = false


func _on_GroundCheck_body_entered(body: Node) -> void:
	var groups = body.get_groups()
	if groups.has("solid"):
		grounded = true
		is_on_wet_ground = groups.has("wet")

func _on_GroundCheck_body_exited(body: Node) -> void:
	if body.get_groups().has("solid"):
		grounded = false
		is_on_wet_ground = false

func _on_RightWallCheck_body_entered(body: Node) -> void:
	if body.get_groups().has("solid"):
		hug_right = true


func _on_RightWallCheck_body_exited(body: Node) -> void:
	if body.get_groups().has("solid"):
		hug_right = false


func _on_LeftWallCheck_body_entered(body: Node) -> void:
	if body.get_groups().has("solid"):
		hug_left = true


func _on_LeftWallCheck_body_exited(body: Node) -> void:
	if body.get_groups().has("solid"):
		hug_left = false


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "fall":
		fell = false

func _on_PickupCollision_area_entered(area):
	emit_signal("picking_node", area)
